package problemsolving1;

import java.util.ArrayList;
import java.util.Scanner;

class CartItem{
	
	private String productName;
	private int numOfProduct;
	private double productPrice;
	
	CartItem(String productName, int numOfProduct, double productPrice)
	{
		this.productName = productName;
		this.numOfProduct = numOfProduct;
		this.productPrice = productPrice;
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getNumOfProduct() {
		return numOfProduct;
	}
	public void setNumOfProduct(int numOfProduct) {
		this.numOfProduct = numOfProduct;
	}
	public double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	
	@Override
	public String toString()
	{
		return String.format(productName + " " + numOfProduct + " " + productPrice);
	}
	
}

public class ShoppingCart {
	

	public static void main(String[] args) {
		
		ArrayList<CartItem> refCI = new ArrayList<CartItem>();
		Scanner refScanner = new Scanner(System.in);
		
		int choice = 0;
		int index = 0;
		
		boolean applicationLoop = true;
		
		while(applicationLoop)
		{
			System.out.println("\nMain Menu - Manage Shopping Cart");
			System.out.println("1. Add item to cart");
			System.out.println("2. Remove item to cart");
			System.out.println("3. View item to cart");
			System.out.println("4. Exit and add item prices");
			System.out.println("5. Empty whole cart");
			System.out.println("6. Exit");
			
			System.out.print("Select an option: ");
			choice = refScanner.nextInt();
			if(choice < 1 || choice > 6)
			{
				System.out.println("Please enter a number from 1 - 6");
			}
			else
			{
				switch(choice)
				{
				case 1:
				{
					String productName;
					int numOfProduct;
					double productPrice;
					System.out.print("\nEnter Item Name: ");
					productName = refScanner.next();
					System.out.print("Enter Quantity: ");
					numOfProduct = Integer.parseInt(refScanner.next());
					System.out.print("Enter price of product: ");
					productPrice = Double.parseDouble(refScanner.next());

					refCI.add(new CartItem(productName, numOfProduct, productPrice));

					break;
				}
				case 2:
				{
					String productName;
					System.out.println("Enter which item to remove: ");
					productName = refScanner.next();
					
					for(int i = 0; i <= refCI.size()-1; i++)
					{
						if(productName.equals( refCI.get(i).getProductName()))
						{
							refCI.remove(refCI.get(i));
							System.out.println("Product successfully removed");
							break;

						}
						
					}

					break;
				}
				case 3:

					refCI.forEach((n)-> System.out.println(n.toString()));

					break;
				case 4:
					double totalPrice = 0.0d;

					for(int i = 0; i <= refCI.size()-1; i++)
					{
						double tempAmount = refCI.get(i).getNumOfProduct() * refCI.get(i).getProductPrice();
						totalPrice += tempAmount;
					}

					System.out.println("\nThe Total Price for your whole cart is: " + totalPrice);
					applicationLoop = false;
					System.exit(1);

					break;
				case 5:

					refCI.clear();

					System.out.println("\nCart has been emptied!");

					break;
				case 6:
					applicationLoop = false;
					System.exit(1);
					break;

				}
			}

		}


	}

}


