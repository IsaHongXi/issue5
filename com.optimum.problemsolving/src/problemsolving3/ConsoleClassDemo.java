package problemsolving3;

import java.io.Console;
import java.util.Scanner;




public class ConsoleClassDemo {

	public static void main(String[] args) {
		
		Console refConsole = System.console();
		User refUser = new User();
		String username = "";
		char[] password;
		
		refConsole.printf("Enter your username: ");
		username = refConsole.readLine();
		refConsole.printf("\nPlease enter your password: ");
		password = refConsole.readPassword();
		
		if(refUser.getPassword().equals(String.copyValueOf(password)) && 
				refUser.getUsername().equals(username))
		{
			refConsole.printf(refUser.getUsername() + "\n");
			refConsole.printf("Password: ");
			for(int i = 0; i < password.length; i++)
			{
				refConsole.printf("x");
			}
		}
		
		else 
		{
			refConsole.printf("\nInformation entered is wrong, please restart programme.");
		}
		
		
		
	}
}

class User{
	
	private String username = "Isa";
	private String password = "password123";
	
	public String getUsername()
	{
		return username;
	}
	
	public String getPassword()
	{
		return password;
	}
	
}
