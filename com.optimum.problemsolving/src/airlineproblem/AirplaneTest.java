package airlineproblem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.Test;

public class AirplaneTest {

	Airplane refA = new Airplane();
	@Test
	public void testCheckFirstReserved() {
		
		assertFalse(refA.checkFirstReserved());
	}

	@Test
	public void testCheckEcoReserved() {
		
		assertFalse(refA.checkEcoReserved());
	}

	@Test
	public void testBookEconomy() {
		
		assertEquals(6, refA.bookEconomy(6));
	}

	@Test
	public void testBookFirstClass() {
		assertEquals(1, refA.bookFirstClass(1));
	}

}
