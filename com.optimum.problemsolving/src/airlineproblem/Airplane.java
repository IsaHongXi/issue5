package airlineproblem;

public class Airplane {

	private boolean[] seatReserved = new boolean[10];
	private int[] seats = {1,2,3,4,5,6,7,8,9,10};

	Airplane()
	{
		for(var i = 0; i < seatReserved.length; i++)
		{
			seatReserved[i] = false;
		}
	}

	//get methods for private arrays
	public boolean[] getReserveArray()
	{
		return seatReserved;
	}

	public int[] getSeat()
	{
		return seats;
	}
	
	public boolean checkFirstReserved()
	{
		for(int i = 0; i < 5; i++)
		{
			if(getReserveArray()[i])
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		
		return true;
	}
	
	public boolean checkEcoReserved()
	{
		for(int i = 5; i < 10; i++)
		{
			if(getReserveArray()[i])
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		
		return true;
	}

	public int bookEconomy(int seatIndex)
	{		
		if(seatIndex > 5 && !getReserveArray()[seatIndex-1])
		{

			getReserveArray()[seatIndex-1] = true;
			displayBoardingPass(seatIndex);
			return seatIndex;

		}
		return -1;
	}

	public int bookFirstClass(int seatIndex)
	{
		if(seatIndex < 6 && !getReserveArray()[seatIndex-1])
		{

			getReserveArray()[seatIndex-1] = true;
			displayBoardingPass(seatIndex);
			return seatIndex;

		}
		
		else
		{
			System.out.println("Seat number "+ seatIndex +" is not available");
		}
		return -1;
	}
	
	public void displayBoardingPass(int seatNumber)
	{
		if(seatNumber < 6)
		{
			System.out.println("First Class");
			System.out.println("Seat number: " + seatNumber);
		}
		else
		{
			System.out.println("Economy Class");
			System.out.println("Seat number: " + seatNumber);
		}
	}
}
