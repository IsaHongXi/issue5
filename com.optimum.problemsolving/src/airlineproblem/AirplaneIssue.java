package airlineproblem;

import java.util.Scanner;

class Application{
	
	public int secondMenu(int menuChoice)
	{
		Scanner refScanner = new Scanner(System.in);
		switch(menuChoice)
		{
		case 1: //economy
			System.out.println("");
			System.out.println("Enter which seat number (1-5)");
			var choice = Integer.parseInt(refScanner.nextLine());
			return choice;
			
			
		case 2: //first class
			System.out.println("");
			System.out.println("Enter which seat number (6-10)");
			var choice2 = Integer.parseInt(refScanner.nextLine());
			return choice2;
			
		}
		return -1;
	}

	public void runApp()
	{
		boolean appLoop = true;
		Airplane refAP = new Airplane();
		Scanner refScanner = new Scanner(System.in);

		while(appLoop)
		{
			System.out.println("Airplane Seat Reservation");
			System.out.println("1. Book First Class Seat");
			System.out.println("2. Book Economy Class Seat");
			System.out.println("3. Exit program");
			System.out.println("Please enter your choice: ");
			var choice = Integer.parseInt(refScanner.nextLine());
			var secondChoice = 0;
			var thirdChoice = "";
			
			switch(choice)
			{
			case 1:
				if(!refAP.checkFirstReserved())
				{
					secondChoice = secondMenu(1);
					refAP.bookFirstClass(secondChoice);
				}
				else
				{
					System.out.println("First class fully reserved, do you want to book Economy? (y/n)");
					thirdChoice = refScanner.nextLine();
					if(thirdChoice.charAt(0) == 'y' || thirdChoice.charAt(0) == 'Y')
					{
						if(!refAP.checkEcoReserved())
						{
							secondChoice = secondMenu(2);
							refAP.bookEconomy(secondChoice);
						}
						else
						{
							System.out.println("Next flight is in 3 hours");
						}
					}
				}
				break;
				
			case 2:
				if(!refAP.checkEcoReserved())
				{
					secondChoice = secondMenu(2);
					refAP.bookEconomy(secondChoice);
				}
				else
				{
					System.out.println("Economy class fully reserved, do you want to book First Class? (y/n)");
					thirdChoice = refScanner.nextLine();
					if(thirdChoice.charAt(0) == 'y' || thirdChoice.charAt(0) == 'Y')
					{
						if(!refAP.checkFirstReserved())
						{
							secondChoice = secondMenu(1);
							refAP.bookFirstClass(secondChoice);
						}
						else
						{
							System.out.println("Next flight is in 3 hours");
						}
					}
				}
				break;
				
			case 3:
				appLoop = false;
				break;
				
				default:
					System.out.println("Please Enter correct option(1-3).");
					break;
			
			}
			
		}

	}
	
}

public class AirplaneIssue {

	public static void main(String[] args) {
		
		Application refApp = new Application();
		refApp.runApp();
	}

}
