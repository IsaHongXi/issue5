package problemsolving2;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class BigDecExampleTest {

	@Test
	public void testTryBigDecimal() {
		
		//test with double 
		assertNotEquals(4.9, BigDecExample.tryBigDecimal(new BigDecimal(2.3d), new BigDecimal(2.6d)));
	}

	
	@Test
	public void test2()
	{
		assertEquals(BigDecimal.valueOf(4.9), BigDecExample.tryBigDecimal(BigDecimal.valueOf(2.3), BigDecimal.valueOf(2.6)));
	}
}
