package problemsolving2;

import java.math.BigDecimal;

public class BigDecExample {
	
	public static BigDecimal tryBigDecimal(BigDecimal first, BigDecimal second) 
	{
		BigDecimal temp = new BigDecimal(0.1d);
		
		temp = first.add(second);
		
		return temp;
	}
	
	public static void BigDecConvert(int temp)
	{
		BigDecimal bgtemp = new BigDecimal(temp);
		System.out.println(bgtemp);
	}
	
	public static void BigDecConvert(long temp)
	{
		BigDecimal bgtemp = new BigDecimal(temp);
		System.out.println(bgtemp);
	}
	
	public static void BigDecConvert(double temp)
	{
		BigDecimal bgtemp = new BigDecimal(temp);
		System.out.println(bgtemp);
	}
	
	
	
	public static void main(String[] args) {
		
		System.out.println("Big Dec: " + BigDecExample.tryBigDecimal(BigDecimal.valueOf(2.3d), BigDecimal.valueOf(2.6d)));
		BigDecConvert(2.4d);
		BigDecConvert(4L);
		BigDecConvert(5);
		
	}

}
